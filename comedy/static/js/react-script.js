class Body extends React.Component {
    constructor(props) {
       super(props);
       this.state = { 
           data : [],

           first_active: 'active',
           second_active: '',
           third_active: '',

           joke_id: 1,
           dadjoke_id: 1,
           tweet_id: 1,

           first_checked: true,
           second_checked: false,
           third_checked: false
        }
    }

    refresh() {
      var model_name, id;
      if (this.state.first_active == 'active') {
        model_name = 'Joke';
        id = this.state.joke_id;
      } else if (this.state.second_active == 'active') {
        model_name = 'DadJokes';
        id = this.state.dadjoke_id;
      } else if (this.state.third_active == 'active') {
        model_name = 'Tweet';
        id = this.state.tweet_id;
      }

      var initial_data = {'model-name': model_name, 'id': id};
      this.postdata(initial_data)
    }

    componentDidMount(){
     this.postdata()
     var self = this;
     
     setInterval(function(){ //get new content after 10 seconds 
        self.refresh() 
      }, 30000);
    }

    update_checked(option) {
        if (option === 1) {
          this.setState({ first_active: 'active', first_checked: true, second_active: '', second_checked: false, third_active: '', third_checked: false })
        } else if (option === 2) {
          this.setState({ first_active: '', first_checked: false, second_active: 'active', second_checked: true, third_active: '', third_checked: false })
        } else if (option === 3) {
          this.setState({ first_active: '', first_checked: false, second_active: '', second_checked: false, third_active: 'active', third_checked: true })
        }
    }

    postdata(initial_data = {'id': 1, 'model-name': 'Joke'}){
        var self = this;
        fetch("\start-jokes\/", {
	        body: JSON.stringify(initial_data),
	        cache: 'no-cache', 
	        credentials: 'same-origin', 
	        headers: {
		        'user-agent': 'Mozilla/4.0 MDN Example',
		        'content-type': 'application/json'
	        },
	        method: 'POST',
	        mode: 'cors', 
	        redirect: 'follow',
	        referrer: 'no-referrer',
	        })
	        .then(response => response.json()).then((json) => {
            var current_data = this.state.data;
            var new_data = json['jokes'].concat(current_data);
            if (initial_data['model-name'] == 'Joke') {
              self.setState({ data : new_data, joke_id: json['ref-id'] }) 
            } else if (initial_data['model-name'] == 'DadJokes') {
              self.setState({ data : new_data, dadjoke_id: json['ref-id'] })
            } else if (initial_data['model-name'] == 'Tweet') {
              self.setState({ data : new_data, tweet_id: json['ref-id'] })
            }
	          
        })
    }

    render(){
       return( 
            <div>
              <div id="side-nav">
                <div className="btn-group btn-group-toggle" data-toggle="buttons">
                  <label className={`btn btn-secondary ${this.state.first_active}`}>
                    <input type="radio" name="options" id="option1" autoComplete="off" value="Joke" ref_id={this.state.joke_id} defaultChecked={this.state.first_checked} onChange={() => this.update_checked(1)} /> Joke
                  </label>
                  <label className={`btn btn-secondary ${this.state.second_active}`}>
                    <input type="radio" name="options" id="option2" autoComplete="off" value="DadJokes" ref_id={this.state.dadjoke_id} checked={this.state.second_checked}  onChange={() => this.update_checked(2)} /> DadJokes
                  </label>
                  <label className={`btn btn-secondary ${this.state.third_active}`}>
                    <input type="radio" name="options" id="option3" autoComplete="off" value="Tweet" ref_id={this.state.tweet_id} checked={this.state.third_checked} onChange={() => this.update_checked(3)} /> Tweet
                  </label>
                </div>
                <div className="col-md-12 top-padding" align="center">
                  <i className="fas fa-sync-alt" onClick={() => this.refresh()}></i>
                </div>
              </div>
            	
	            {this.state.data.length == 0 && 
	               <div> No options available.</div>
	            }
	            {this.state.data.length > 0 && 
	              <div className="container top-padding" id="jokes">
	                   {this.state.data.map(function(item,i){
	                      return(
	                            <div key={item['key']} className="card col-md-7">
	                                <div className="card-body">
	                                    {item['text']} 
	                                <p><span className="badge badge-secondary">{item.name}</span></p>
	                                </div>
	                            </div>
	                      )
	                   })}
	               </div>
	            }
          	</div>
         )
    }
}
// ========================================

ReactDOM.render(
  <Body />,
  document.getElementById('root')
);
