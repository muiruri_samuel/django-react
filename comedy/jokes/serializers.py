from rest_framework import serializers
from jokes.models import DadJokes, Tweet, Joke

class DadJokesSerializer(serializers.Serializer):
    class Meta:
        model = DadJokes
        fields = ('id', 'text')

class TweetSerializer(serializers.Serializer):
    class Meta:
        model = Tweet
        fields = ('id', 'text')

class JokeSerializer(serializers.Serializer):
    class Meta:
        model = Joke
        fields = ('id', 'text')